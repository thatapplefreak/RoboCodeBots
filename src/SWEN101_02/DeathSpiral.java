package SWEN101_02;

import robocode.*;

import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * DeathSpiral - Moddified CirclingBot that only fires on targets that are likely to be hit
 *
 * @author Byron Zaharako
 * @author Ryan Malley
 * @author Austin Wolf
 *
 *
 */
public class DeathSpiral extends Robot {

    int moveDirection = 1;

    public void run() {
        // Set colors
        setColors(Color.RED, Color.BLUE, Color.WHITE, Color.PINK, Color.CYAN);
        while (true) {
            ahead(100*moveDirection); // Move ahead 100
            turnGunRight(360); // Spin gun around
        }

    }

    @Override
    public void onHitWall(HitWallEvent event) {
        if (moveDirection == 1) {
            moveDirection = -1;
        } else if (moveDirection == -1) {
            moveDirection = 1;
        }
    }
    public void onScannedRobot(ScannedRobotEvent e) {
        if (e.getDistance() < 50) {
            fire(3);
        }
        if (e.getDistance() < 400) {
            if (e.getVelocity() == 0 && getEnergy() > 15) {
                fire(3);
            } else {
                fire(1);
            }
        }
    }

    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        turnLeft(90 - e.getBearing());
    }
}
